package inter.man;

import inter.Man;

public interface Engineer extends Man {
    void writeCode();
}

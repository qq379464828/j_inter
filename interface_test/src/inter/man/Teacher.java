package inter.man;

import inter.Man;

public interface Teacher extends Man {
    void teach();
}

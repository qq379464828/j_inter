package inter;

public interface Man {

    static final int sex = 1;

    void work();

    default void happy() {
        System.out.println("be happy");
    }

}

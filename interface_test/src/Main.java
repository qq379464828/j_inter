import cls.User;
import inter.man.Engineer;

public class Main {
    public static void main(String[] args) {
        Engineer abc = new User();
        abc.work();
        abc.happy();
//        abc.teach();
        abc.writeCode();
    }
}

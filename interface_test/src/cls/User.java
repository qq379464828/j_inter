package cls;

import inter.man.Engineer;
import inter.man.Teacher;

public class User implements Engineer, Teacher {
    @Override
    public void writeCode() {
        System.out.println("User.writeCode");
    }

    @Override
    public void teach() {
        System.out.println("User.teach");
    }

    @Override
    public void work() {
        System.out.println("User.work");
    }
    
}
